import {
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

export function daysBetween(
    start: LocalDate,
    end: LocalDate,
): number {
    return end.dayOfYear() - start.dayOfYear();
}

export function afterIntervalTimes(
    start: LocalDate,
    interval: Period,
    multiplier: number,
): LocalDate {
    let begin = start;
    for (let i = 0; i < multiplier; ++ i)
        begin = begin.plus(interval);

    return begin;
}

export function recurringEvent(
    start: LocalDateTime,
    end: LocalDateTime,
    interval: Period,
    timeOfDay: LocalTime,
): LocalDateTime[] {
    let begin = start;
    const hold: LocalDateTime[] = [];

    if (begin.toLocalTime().isAfter(timeOfDay))
        begin = begin.plusDays(1);

    begin = begin.withHour(timeOfDay.hour());
    begin = begin.withMinute(timeOfDay.minute());

    while (begin.isBefore(end)) {
        hold.push(begin);
        begin = begin.plus(interval);
    }

    return hold;
}